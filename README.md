![logo](resources/coollogo_com-168992182.png)

# new-c | A cli tool to `touch` some C #

Nothing like coding some good old c with nothing but a text editor, right?

Don't you just love writing the same 3 `#include`s, an `#ifndef` block, and a main function header?  Over and over and over...

Lost souls, you are found.  

Welcome to `new-c`.

## Installation

`npm install -g new-c`

## Usage

`new-c --help`

yields

```
  Usage: new-c [options] [module-id]

  Generate an empty `.c/.h` file pair.

  Options:

    -h, --help  output usage information
    --force  write to target files even they already exists
```

## Output

A `.c`/`.h` file pair will be created.

### `.c` has: 

* an include for the matching `.h`
* essential includes:
    * `stdio.h` 
    * `stddef.h` (`NULL`)
    * `stdlib.h` (`malloc`, `atoi`, `exit`, ...)
* an empty main method.

### `.h` has:

* A standard include once `#ifndef` block.

## TODO

* Evolve into a complicated and cumbersome framework.
* Use standard in as alternative body contents.
  * Infer imports for common import needs (such as pthread, semaphore, wait.)
* Add options for common include sets, with comments that include linkage notes.

## Citations

Today's episode was brought to you, in part, by:

* The letter [tutorial](https://developer.atlassian.com/blog/2015/11/scripting-with-node/).
* Too much copy and pasting.
* An $80 dollar laptop.
* And the shoulders of giant robots.

## Farewell, for now...

Ya'll come back now.

:: The Jolly Wizard