#!/usr/bin/env node

var DEFAULT_FILE = 'main'

console.log("\n## Parsing Arguments ##\n")

var commander = require('commander')

commander
	.description("Generate an empty `.c/.h` file pair.")
	.option("--force", "write to target files even they already exists")
	.arguments("[module-id]")
	.action(upgradeArgs)
	.parse(process.argv)

function upgradeArgs(id) {mod=id}

if (!commander.args.length || !mod) {
	console.log("\t ): missing module-id \n")
	process.exit(1);
}
console.log("module-id : %s", mod)

console.log('\n## Generating file code ##\n');

function genC()
{
    var r = "";
    
    r += '/* Module header */ \n'
    r += '#include "' + mod + '.h" \n'
    r += '\n'
    r += '/* Common standard libraries */ \n'
    r += '#include <stdio.h> \n';
    r += '#include <stddef.h> \n';
    r += '#include <stdlib.h> \n';
    
    r += '\n'
    r += 'void main (int argc, char* argv[])\n{\n\n}\n'
    
    return r
}

function genH()
{
    var r = ''
    
    var m = mod + '_h'
    
    r += '#ifndef '+ m + ' \n'
    r += '#define '+ m + ' \n'
    r += '\n'
    r += '#endif /* ' + m + ' */ \n'
    
    return r;
}

cfile = mod + '.c'
hfile = mod + '.h'

var c = genC()
var h = genH()

console.log('\n### %s ###\n', hfile);
console.log(h)

console.log('\n### %s ###\n', cfile);
console.log(c)

console.log('\n## Writing files ## \n')

doFile(cfile, c)
doFile(hfile, h)

function doFile(file, data)
{
    console.log(file);
    var fs = require('fs')
    if (!fs.existsSync(file))
	    fs.writeFileSync(file, data)
    else
    {
	    console.log("\t ): File already exists (--force required)")

	    if (commander.force)
	    {
		    console.log(' \t |: Detected: [--force] writing to file')
    	    fs.writeFileSync(file, data)
	    }
    }
}
